class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tweets, dependent: :destroy

  has_many :active_relationships, class_name: "Relationship", foreign_key: "follower_id", dependent: :destroy
  has_many :passive_relationships, class_name: "Relationship", foreign_key: "followed_id", dependent: :destroy

  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  # helper methods

  # seguir usuari
  def follow(altre)
    active_relationships.create(followed_id: altre.id)
  end

  # deixar de seguir
  def unfollow(altre)
    active_relationships.find_by(followed_id: altre.id).destroy
  end

  # esta seguint?
  def following?(altre)
    following.include?(altre)
  end

end
