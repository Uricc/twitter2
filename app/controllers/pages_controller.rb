class PagesController < ApplicationController
  # Codi back-end per a pages/index
  def index
  end

  # Codi back-end per a pages/home
  def home

  end

  # Codi back-end per a pages/profile
  def profile

  end

  # Codi back-end per a pages/explore
  def explore

  end
end
